Release Log
===========

18 May 2020: PT 2.4.3
--------------------------------

+ CC: fixed a rare occurrence of the panel crashing PT2 on some Photoshops
+ CC: an option to hide/show gif helpers in the flyout menu (CC2014+). Gifs were causing slowdowns on Mac Catalina

19 Dec 2019: PT 2.4.2
--------------------------------

+ CC: Installers were updated to work with Photoshop 2020
+ CC: small fixes
+ CS6: fixed de-perspective not working in some cases (again)
+ Added EULA

9 May 2019: PT 2.4.1
--------------------------------

+ CS6: fixed de-perspective not working in some cases
+ CC: slight changes in UI

25 Nov 2018: PT 2.4.0
--------------------------------

+ Possible to recreate paths for newly created Warp and Deperspective layers using Fix Paths command
+ Deperspective can unwarp 2-points path
+ When no paths and two grids are presented, Deperspective will unwarp the whole document
+ When Horizon and Y grid are presented, Deperspective will unwarp the document to 2p perspective
+ Installers updated for CC2019
+ Some fixes

22 Mar 2018: PT 2.3.0
----------------------------------

+ Parallel and distant grids are more memory-efficient
+ It's possible to create two drids from one 4-points quadrangle path
+ Create ``High Density Grids`` from selection
+ New manual: http://pt2.readthedocs.io/
+ Some fixes

16 Nov 2017: PT 2.2.3
----------------------------------

+ Panel is resizable once again in PS CC2018

24 May 2017: PT 2.2.2
----------------------------------

+ New grids respect default grid color, set in ``Ctrl/Cmd+Create Grid`` menu
+ Grid Width is a multiplier instead of absolute value
+ Fixed broken tip-gif for ``Change Points Order``

14 Apr 2017: PT 2.2.1
----------------------------------

+ fixed ``Recolor`` error in CS6 version

28 Mar 2017: PT 2.2.0
----------------------------------

+ New ``De-Perspective`` algorithm, works in CS6
+ ``Fix Paths`` and ``Autowarp`` work with Parallel grids
+ Create VPs on Horizon line with 1 path
+ All functions are available as scripts, assign them to hotkeys or use on Brusherator
+ Change new Horizon density with ``Ctrl/Cmd-Click`` on Horizon button
+ Change grid color to custom with ``Ctrl/Cmd-Click`` on ``Recolor`` button
+ Creating grids is 20-50% faster
+ probably fixed something

26 Aug 2016: PT 2.1.0
----------------------------------

+ CS6 version
+ New warp mode — Autowarp: use ``Warp`` with 2 visible Persp layers and no paths
+ ``Fix Path`` now works with multiple paths
+ ``Warp`` now works with multiple paths
+ ``Fix Paths`` now can work with 2-points paths
+ ``Change Points Order`` shows points order
+ Mirroring paths (``Shift+Change Points Order``) works differently
+ Warped layers keep opacity/blending modes
+ Fixed ``Bezier Warp`` weird document canvas changes
+ Fixed weird results with ``De-perspective`` when path points are outside of active document boundaries 
+ Fixed reset of perspective layer color when recreating a grid layer
+ ``Alt+Click`` opens correct video link instead of ‘Never Gonna Give You Up’ by Rick Astley

18 Jul 2016: PT 2.0.2
----------------------------------

+ Fixed ``Fix Paths`` which I broke in 2.0.1

18 Jul 2016: PT 2.0.1
----------------------------------

+ Fixed ``Re-color`` for users of non-English Photoshop
+ Remote VPs don’t create huge files (‘Can’t save .psd file because it’s larger than 2GB’ error)

10 Jul 2016: PT 2.0
----------------------------------

+ initial release

.. |br| raw:: html

   <br />
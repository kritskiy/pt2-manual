.. _horizon:

Horizon
======================

.. image:: img/hor.png

This function will allow you to define horizon line which can be sometimes helpful.

--------------------------------

Pen Tool
--------------------------------

To define grids, ``Pen Tool`` is used. Use it in ``Path`` mode. Learn more in :ref:`Interface and Settings <pentool>` section.

.. image:: ../img/pen.gif

--------------------------------

Horizon from one point
--------------------------------

#. Make one path point
#. Hit ``Horizon`` button

Resulting horizon line will be horizontal (ba dum tssss)

.. image:: img/h1.gif

--------------------------------

Horizon from two points
--------------------------------

#. Make one path with two path points
#. Hit ``Horizon`` button

Resulting horizon line will be between two points

.. image:: img/h2.gif

--------------------------------

Horizon from perspective grids
--------------------------------

#. Select one or two perspective grid layers
#. Hit ``Horizon`` button

Resulting horizon line will be on vanishing point if one perspective layer was selected ot between two vanishing points, if there were two layers selected

.. image:: img/h3.gif

--------------------------------



.. |br| raw:: html

   <br />
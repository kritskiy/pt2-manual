Reset Smart Object
======================

.. image:: img/icon.png

This function will reset transformations of an active smart object layer.

#. Select one layer with smart object
#. Hit ``Reset Smart Object`` button

.. image:: img/reset1.gif

.. |br| raw:: html

   <br />
Warp
======================

.. image:: img/icon.png

This function will warp selected layer to a plane, defined by a quadrangle.

Pen Tool
--------------------------------

To define grids, ``Pen Tool`` is used. Use it in ``Path`` mode. Learn more in :ref:`Interface and Settings <pentool>` section.

.. image:: ../img/pen.gif

--------------------------------

Warping a layer
--------------------------------

#. Make one or several quadrangle paths: note that point order is important, :ref:`read more <orderFunction>`
#. Realign them automatically if necessary (see below)
#. Hit ``Warp`` button
#. Resulting layer is a Smart Object, hitting ``Ctrl/Cmd+T`` for free transorm will allow to further modify it in perspective.

.. image:: img/warp1.gif

--------------------------------

Aligning a quadrangle within grids
---------------------------------------

Use a :ref:`Fix Paths <fixFunction>` function to quickly align active quadrangle paths with existing perspective or parallel grids

.. image:: img/warp2.gif

--------------------------------

Aligning warped layer
--------------------------------

Warped layer will be made a smart object, which means that it can be further transformed in perspective and original layer can be changed

.. image:: img/warp3.gif

--------------------------------

Auto Warp
--------------------------------

If two grid layers are visible, it's possible to use ``Warp`` without making a quadrangle path. Note that it's usually necessary to modify a resulting layer.

* If you hold ``Ctrl/Cmd`` while using automatic ``Warp``, only warp-path is created.

.. image:: img/warp4.gif

.. |br| raw:: html

   <br />
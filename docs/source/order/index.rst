Change Points Order
======================

.. _orderFunction:

This function will change points order for active paths. Point order is important for ``Wrap`` and ``De-perspective`` functions: they define how warped image is going to be rotated in perspective. Points usually go clock-wise, from top left corner to bottom-left: first point is associated with top left corner of a layer, second — with top right corner, third is bottom right point and fourth is bottom left.

.. image:: img/order.png

Notice how this sign is rotated depending on which point in quadrange was placed first. Also notice that the last sign is mirrored: points were placed in counter-clock order.

.. image:: img/order2.png

Reordering points CW
---------------------------------------

#. Hit ``Change Order`` button
#. Points order will shift clockwise
#. New points order will be displayed for a moment

.. image:: img/order1.gif

--------------------------------

Reordering points CCW
---------------------------------------

#. Hold ``Ctrl/Cmd`` when hitting  ``Change Order`` button
#. Points order will shift counter-clockwise
#. New points order will be displayed for a moment

.. image:: img/order2.gif

--------------------------------

Mirroring points
---------------------------------------

#. Hold ``Shift`` when hitting ``Change Order`` button
#. Points order will change direction
#. New points order will be displayed for a moment

.. image:: img/order3.gif

--------------------------------

.. |br| raw:: html

   <br />
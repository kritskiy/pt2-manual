Recolor
======================

.. image:: img/icon.png

This function will cycle between several grid colors.

#. Select a grid or horizon layer
#. Hit ``Recolor``

Grid color will cycle between black, red (with ``_x`` suffix), green (``_y``) and blue (``_z``);

.. image:: img/recolor.gif

--------------------------------

.. |br| raw:: html

   <br />
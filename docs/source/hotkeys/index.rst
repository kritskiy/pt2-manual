Assigning functions to hotkeys
========================================

To assign functions to hotkeys

#. Go to ``Edit > Keyboard Shortcuts`` Photoshop menu
#. Make sure that ``Shrotcuts For:`` is set to ``Applications Menu``
#. Find PT2 scripts under ``File > Scripts``
#. And assign shortcuts

.. img/hotkeys.png

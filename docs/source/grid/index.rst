Create Grid
======================

.. image:: img/icon.png

Creating perspective and parallel grids is one of the main functions of PT2.

Basics
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pen Tool
--------------------------------

To define grids, ``Pen Tool`` is used. Use it in ``Path`` mode. Learn more in :ref:`Interface and Settings <pentool>` section.

.. image:: ../img/pen.gif

--------------------------------

There're several different ways grids can be created.

Perspective Grid: VP from one point
-------------------------------------------------------------

#. Make one path point
#. Hit ``Grid`` button

Resulting grid will have a vanishing point in the path point.

.. image:: img/newgrid.gif

--------------------------------

Perspective Grid: Distant VP
--------------------------------

#. Make one path with two path points
#. Make a second path with two path points
#. Hit ``Grid`` button

Resulting grid will have a vanishing point in the intersection of two paths.

.. image:: img/newgrid2.gif

--------------------------------

Perspective Grid: Two distant VPs
-----------------------------------

When reconstructing a perspective in a photo, quite often there're visible rectangular surfaces. In this case it could be easy to create two perspective grids in one go.

#. Make one path with four path points (quadrangle)
#. Hit ``Grid`` button

Resulting two grids will have vanishing points in the intersections of two pairs of the paths.

.. image:: img/newgrid3.gif

--------------------------------

Perspective Grid: Intersection with Horizon
-------------------------------------------------------------

If :ref:`Horizon<horizon>` layer is presented in the document, one path is enough for creating a perspective grid.

#. Make one path with two path points
#. Hit ``Grid`` button

Resulting grid will have a vanishing point in the intersection of the path and horizon line.

.. image:: img/newgrid4.gif

--------------------------------

Parallel Grid
--------------------------------

#. Make one path with two path points
#. Hit ``Grid`` button

Resulting grid will be a parallel lines grid.

.. image:: img/newgrid5.gif

--------------------------------

Parallel Grid: Specific distance
--------------------------------

#. Make one path with two path points
#. Copy-Paste the path or use ``Ctrl/Cmd+Alt+Drag`` to make a clone and move it to a desirable distance
#. Hit ``Grid`` button

Resulting grid will be a parallel lines grid with the same distance as betwen the clones.

.. image:: img/newgrid6.gif

--------------------------------

Advanced
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Additional options
--------------------------------

Holding ``Cmd/Ctrl`` when clicking ``Grid`` will show a window with additional options like grid color, density, layer opacity and line width.

.. image:: img/newgrid7.gif

--------------------------------

Recreate grid
--------------------------------

If you have a grid layer selected, hitting ``Grid`` button will recreate the grid. This is useful if canvas size or image size have changed or settings of an existing grid must be modified.

.. image:: img/newgrid8.gif

--------------------------------

High Density area
--------------------------------

If selection is present when clicking ``Grid``, selected area will get x3 times more density. Recreate grid to get rid of high density area.

.. image:: img/newgrid9.gif

--------------------------------

.. |br| raw:: html

   <br />
Perspective Tools 2 manual
==========================

Perspective Tools is an extension panel for Adobe Photoshop from CS6 to the latest, used for 

* automatically creating perspective and parallel grids;
* managing them;
* warping layers to perspective;
* unwarping distorted layers to rectangles;
* and more!

.. image:: img/pt2.gif


Contact me at kritskiy.sergey@gmail.com |br|\
Twitter: `@ebanchiki <https://twitter.com/ebanchiki>`_ |br|\
Grab the extension on `Gumroad <https://gum.co/PT2>`_ or `Cubebrush <http://cbr.sh/yae0m>`_.

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   ui/index
   grid/index
   horizon/index
   fill/index
   visibility/index
   warp/index
   depersp/index
   order/index
   fix/index
   reset/index
   hotkeys/index
   notes/index
   
.. |br| raw:: html

   <br />
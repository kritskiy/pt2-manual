Toggle Visibility
======================

.. image:: img/icon.png

This function will toggle visibility of grid layers (perspective, parallel and horizon). This function works best when assigned to a hotkey.

If you want to disable Toggle Visibility for a layer, assign any color label to it.

.. image:: img/visibility.gif

--------------------------------

.. |br| raw:: html

   <br />
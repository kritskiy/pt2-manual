Interface and Settings
======================

Photoshop CC version of the panel consists of two tabs: **Grids** and **Warps**, with relevant functions. It can be resized into three different ways:

	* larger window with little help-gifs on the bottom;
	* smaller vertical window
	* smaller horizontal window

.. гиф с новым уи
.. image:: img/ui.gif

.. _helpers:

Update 2.4.3 includes an option in Flyout menu to show/hide the gif helpers: this option is disabled by default:

.. image:: img/helpers.jpg

--------------------------------

CS6 version of the panel has all the functions but no tabs and non-resizable window

.. image:: img/cs6.png

``Alt+Click`` on any of the buttons will open a Help page for corresponding function.

------------------------------------------------------

.. _pentool:

Using Photoshop Pen Tool
--------------------------------

To define grids and rectangles mostly ``Pen Tool`` is used. Use it in ``Path`` mode. You can adjust points location with keyboard arrows.

.. image:: ../img/pen.gif

--------------------------------

To create disconected paths, ``Cmd/Ctrl+Click`` on an empty space to deselect active path and start a new one.

.. image:: ../img/pen_several.gif

--------------------------------

To create path duplicate, hold ``Cmd/Ctrl+Alt`` and drag active path.

.. image:: ../img/pen_duplicate.gif

.. |br| raw:: html

   <br />
Fix Paths
======================

.. _fixFunction:

.. image:: img/icon.png

This function will align paths using visible perspective or parallel grids and will restore previous paths for existing Warp and Deperspective layers. Aligning quadrangle paths is useful for ``Warp`` and ``Depespective`` functions, aligning linear paths is useful for further stroking. 

Aligning 4-points quadrangle paths
-----------------------------------

#. Select a Path Object with one or more 4-points quadrangle paths
#. Make sure two grid layers of the same type are visible
#. Hit ``Fix Paths`` button

.. image:: img/fix1.gif

-----------------------------------

Aligning 2-points linear paths
-----------------------------------

#. Select a Path Object with one or more 2-points linear paths
#. Make sure one or two grid layers are visible
#. Hit ``Fix Paths`` button

.. image:: img/fix2.gif

--------------------------------

Restoring previous warp path
--------------------------------

After update 2.4.0 ``Fix Paths`` can restore paths that were used to create Warp and Deperspective layers. Note that this will work only with layers created with 2.4.0 and newer.

#. Select a warped or deperspectified layer
#. Make sure no paths are selected
#. Hit ``Fix Paths``

An example of restoring of a warp path of a deperspective layer and using it to create new grids:

.. image:: img/fix3.gif

--------------------------------

.. |br| raw:: html

   <br />
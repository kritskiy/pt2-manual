Bake Layers to Normal
======================================

Usage
~~~~~~

This function will bake any layer to a Normal blending layer. Any pixel from selected layers that is different from underlying pixels will eb baked to 100% opacity normal pixel. This is mostly useful when you need to merge together several layers of different blending mode just to have them on a separate layer. Or when merging the layers with Blend If... option.

.. image:: img/start.png

* Hold ``Alt`` to reverse Close On Click option 

--------

Use cases
~~~~~~~~~~

Example of baking several Solid Fill layers with Blend If... option, they can be merged afterwards

.. image:: img/uc1.png

.. |br| raw:: html

   <br />
De-perspective
======================

.. image:: img/icon.png

This function will attempt to unwarp the current document or its portion, defined by a quadrangle path, to rectangular layer.

Pen Tool
--------------------------------

To define grids, ``Pen Tool`` is used. Use it in ``Path`` mode. Learn more in :ref:`Interface and Settings <pentool>` section.

.. image:: ../img/pen.gif

--------------------------------

Unwarping a layer
--------------------------------

#. Make one quadrangle path: note that point order is important, :ref:`read more <orderFunction>`
#. Realign it automatically if necessary (see below)
#. Hit ``De-perspective`` button
#. Quadrangle portion of the image will be converted to rectangular Smart Object. Note that bottom layer (``cont``) may be quite blurry, it's best to be used for references only and it should be switched off if not needed
#. Since resulting layer is a Smart Object, hitting ``Ctrl/Cmd+T`` for free transorm will allow to further modify it in perspective.

.. image:: img/deperp.gif

--------------------------------

Aligning a quadrangle within grids
---------------------------------------

Use a :ref:`Fix Paths <fixFunction>` function to quickly align active quadrangle paths with existing perspective or parallel grids

.. image:: img/fix.gif

--------------------------------

Unwarping a document
--------------------------------

If the current document has two visible perspective layers and no path for deperspective, the whole document will be unwarped. This can be useful to extract quadrangle details or textures for further modifying them without creating paths new smart objects.

.. image:: img/deperp2.gif

Converting a 3-points perspective to 2-points
----------------------------------------------------------------

If the current document has a visible Y perspective layer and two visible perspective layers OR a :ref:`Horizon<horizon>` layer, the document will be unwarped in a way so Y grid will become vertical, eliminating the vertical contraction. 

.. image:: img/deperp3.gif

Note that it's possible to restore the path that was used to unwarp the document (using :ref:`Fix Paths<fixFunction>` function) and re-warp it to the original perspective

.. image:: img/deperp4.gif

.. |br| raw:: html

   <br />
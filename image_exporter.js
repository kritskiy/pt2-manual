/* beautify preserve:start */
#include "/Users/sergeykritskiy/Dropbox (Personal)/shared_from_win/Sync/PS/Scripts/libraries/utilities.jsx"
#include "/Users/sergeykritskiy/Dropbox (Personal)/shared_from_win/Sync/PS/Scripts/libraries/documents.jsx"
#include "/Users/sergeykritskiy/Dropbox (Personal)/shared_from_win/Sync/PS/Scripts/libraries/layers.jsx"
/* beautify preserve:end */

function main() {

    var clone = D.clone(),
        l = L.getInfo()[0],
        myName = getName(l.index),
        groupName = myName.split('/'),
        savePath = "/Users/sergeykritskiy/Dropbox (Personal)/shared_from_win/Sync/PS/git/bakery_manual/docs/source/";

    if (groupName.length == 1) {
        groupName.unshift('')
    };

    L.mergeDown();

    L.loadSelection();

    D.cropToSelection();

    D.flatten();

    clone.getSize().resize({ w: 690 });

    D.savePNGQuant({ name: groupName[1], path: savePath + '/' + groupName[0] + '/img' });

    clone.close();

    U.copyToClipboard('.. image:: img/' + groupName[1] + '.png');

    function getName(_i) {
        if (L.checkIfGroup(_i)) {
            L.selectByIndex(_i)
            return L.getName(_i);
        } else {
            return getName(_i + 1);
        }
    };

}

app.activeDocument.suspendHistory("temp", "main()");